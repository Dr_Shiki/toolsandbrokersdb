﻿using System;
using System.Collections.Generic;
using System.Text;
using PrimatLab_project.Models;
using SQLite.CodeFirst;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Data.Entity.Infrastructure;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace PrimatLab_project.Context
{
    public class LocalContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=tools_brokers.db");
            base.OnConfiguring(optionsBuilder);
        }

        private void TrackDates()
        {
            //Here's a place where trigger may be implemented. But I decided to work aroung logic in BrokerModel
        }

        public override int SaveChanges()
        {
            //TrackDates();
            return base.SaveChanges();
        }

        public DbSet<BrokerModel> Brokers { get; set; }
        public DbSet<ToolModel> Tools { get; set; }
    }
}