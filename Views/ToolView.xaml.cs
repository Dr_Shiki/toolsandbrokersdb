﻿using PrimatLab_project.Context;
using PrimatLab_project.Models;
using PrimatLab_project.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrimatLab_project.Views
{
    /// <summary>
    /// Interaction logic for ToolView.xaml
    /// </summary>
    public partial class ToolView : Window
    {
        public delegate void DialogClosed();
        public event DialogClosed Applied;

        public ToolView(ToolModel tool, LocalContext context)
        {
            InitializeComponent();
            TitleBorder.MouseLeftButtonDown += delegate { DragMove(); };
            DataContext = new ToolViewModel(tool, context);
            (DataContext as ToolViewModel).Applied += ToolView_Applied;
        }

        private void ToolView_Applied()
        {
            Applied?.Invoke();
        }
    }
}
