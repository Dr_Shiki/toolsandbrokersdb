﻿using Microsoft.EntityFrameworkCore;
using PrimatLab_project.Commands;
using PrimatLab_project.Context;
using PrimatLab_project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace PrimatLab_project.ViewModels
{
    class ToolViewModel : ViewModelBase
    {
		public delegate void DialogClosed();
		public event DialogClosed Applied;

		private readonly LocalContext _context;
		private ToolModel _tool;
		public ToolModel Tool
		{
			get
			{
				return _tool;
			}
			set
			{
				_tool = value;
				OnPropertyChanged(nameof(Tool));
			}
		}

		private string _ticker;
		public string Ticker
		{
			get
			{
				return _ticker;
			}
			set
			{
				_ticker = value;
				_tool.Ticker = _ticker;
				OnPropertyChanged(nameof(Ticker));
			}
		}

		private string _type;
		public string Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
				_tool.Type = _type;
				OnPropertyChanged(nameof(Type));
			}
		}

		private double _lastPrice;
		public double LastPrice
		{
			get
			{
				return _lastPrice;
			}
			set
			{
				_lastPrice = value;
				_tool.LastPrice = _lastPrice;
				OnPropertyChanged(nameof(LastPrice));
			}
		}

		private int _volume;
		public int Volume
		{
			get
			{
				return _volume;
			}
			set
			{
				_volume = value;
				_tool.Volume = _volume;
				OnPropertyChanged(nameof(Volume));
			}
		}

		public ICommand ApplyCommand { get; }
		public ICommand CancelCommand { get; }

		public ToolViewModel(ToolModel subj, LocalContext context)
		{
			_context = context;
			_context.Tools.Load();

			if (subj == null)
			{
				_tool = new ToolModel();
				SubstituteNullToolModel();
				ApplyCommand = new RelayCommand(obj => { _context.Add(_tool); _tool.BrokerModel.UpDateVolume(); _context.SaveChanges(); }, canBeApplied);
			}
			else
			{
				_tool = _context.Tools.FirstOrDefault(x => x == subj);
				SubstituteExistingToolModel(subj);
				ApplyCommand = new RelayCommand(obj => { _context.SaveChanges(); _tool.BrokerModel.UpDateVolume(); Applied?.Invoke(); }, canBeApplied);
			}
		}

		private void SubstituteExistingToolModel(ToolModel tool)
		{
			_ticker = tool.Ticker;
			_type = tool.Type;
			_volume = tool.Volume;
			_lastPrice = tool.LastPrice;
		}

		private void SubstituteNullToolModel()
		{
			_ticker = "";
			_type = "";
			_volume = 0;
			_lastPrice = 0;
		}

		private bool canBeApplied(object parameter)
		{
			ToolModel tool = parameter is ToolModel ? (ToolModel)parameter : null;
			if (tool == null)
				return false;
			return !(tool.Type == null || tool.Ticker == null || tool.Ticker == "" || tool.Type == "");
		}
	}
}
