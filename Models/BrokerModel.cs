﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace PrimatLab_project.Models
{
    [Table("Brokers")]
    public class BrokerModel
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [DefaultValue(0)]
        public double Volume { get; set; }
        [DefaultValue(false)]
        public bool AccessFlag { get; set; }
        [Required]
        public List<ToolModel> Tools { get; set; }

        public void UpDateVolume()
        {
            Volume = Tools.Sum(x => x.Volume * x.LastPrice);
        }
    }
}
