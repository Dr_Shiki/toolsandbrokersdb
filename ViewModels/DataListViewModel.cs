﻿using Microsoft.EntityFrameworkCore;
using PrimatLab_project.Commands;
using PrimatLab_project.Context;
using PrimatLab_project.Models;
using PrimatLab_project.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace PrimatLab_project.ViewModels
{
    class DataListViewModel : ViewModelBase
    {
        public delegate void EntityModified();

        public event EntityModified BrokerModified;
        public event EntityModified ToolModified;

        private readonly LocalContext _context;

        private readonly ObservableCollection<BrokerModel> _brokersList;

        private readonly ObservableCollection<ToolModel> _toolsList;

        public IEnumerable<BrokerModel> BrokersList => _brokersList;

        public IEnumerable<ToolModel> ToolsList => _toolsList;

        private ToolModel _selectedTool;
        public ToolModel SelectedTool
        {
            get
            {
                return _selectedTool;
            }
            set
            {
                _selectedTool = value;
                OnPropertyChanged(nameof(SelectedTool));
            }
        }

        private BrokerModel _selectedBroker;
        public BrokerModel SelectedBroker
        {
            get
            {
                return _selectedBroker;
            }
            set
            {
                _selectedBroker = value;
                OnPropertyChanged(nameof(SelectedBroker));
            }
        }

        public ICommand AddBrokerCommand { get; }
        public ICommand RemoveBrokerCommand { get; }
        public ICommand EditBrokerCommand { get; }
        public ICommand AddToolCommand { get; }
        public ICommand RemoveToolCommand { get; }
        public ICommand EditToolCommand { get; }

        public DataListViewModel()
        {
            _context = new LocalContext();
            _context.Database.Migrate();

            _brokersList = new ObservableCollection<BrokerModel>();
            _toolsList = new ObservableCollection<ToolModel>();

            _context.Tools.Load();
            _toolsList = _context.Tools.Local.ToObservableCollection();
            _context.Brokers.Load();
            _brokersList = _context.Brokers.Local.ToObservableCollection();


            //It seems to me, that passin' context from one view to another brakes MVVM approach and links the views.
            //But for now I see no other way to maintain PropsChangeNotification, the code is to be refactored!

            AddBrokerCommand = new RelayCommand(obj =>
            {
                BrokerView view = new BrokerView(null, _context);
                view.Show();
            });

            EditBrokerCommand = new RelayCommand(obj =>
            {
                BrokerView view = new BrokerView(_selectedBroker, _context);
                view.Applied += onBrokerModified;
                view.Show();
            }, e => _selectedBroker != null);

            RemoveBrokerCommand = new RelayCommand(obj =>
            {
                _context.Brokers.Remove(_selectedBroker);
                _context.SaveChanges();
            }, e => _selectedBroker != null);

            AddToolCommand = new RelayCommand(obj =>
            {
                ToolView view = new ToolView(null, _context);
                view.Show();
            });

            EditToolCommand = new RelayCommand(obj =>
            {
                ToolView view = new ToolView(_selectedTool, _context);
                view.Applied += onToolModified;
                view.Show();
            }, e=> _selectedTool != null);

            RemoveToolCommand = new RelayCommand(obj =>
            {
                _context.Tools.Remove(_selectedTool);
                _context.SaveChanges();
            }, e => _selectedTool != null);
        }

        private void onBrokerModified()
        {
            BrokerModified?.Invoke();
        }
        private void onToolModified()
        {
            ToolModified?.Invoke();
        }
    }
}
