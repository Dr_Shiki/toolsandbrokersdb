﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PrimatLab_project.Models
{
    [Table("Tools")]
    public class ToolModel
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Ticker { get; set; }
        [Required]
        public string Type { get; set; }
        [DefaultValue(0)]
        public double LastPrice { get; set; }
        [DefaultValue(0)]
        public int Volume { get; set; }
        public virtual BrokerModel BrokerModel { get; set; }
        [DefaultValue(false)]
        public bool IsOwned { get; set; }
    }
}
