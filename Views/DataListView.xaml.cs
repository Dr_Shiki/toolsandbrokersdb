﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrimatLab_project.Views
{
    /// <summary>
    /// Interaction logic for DataListView.xaml
    /// </summary>
    public partial class DataListView : UserControl
    {
        public DataListView()
        {
            InitializeComponent();
            Context.BrokerModified += onBrokerModified;
            Context.ToolModified += onToolModified;
        }

        void onBrokerModified()
        {
            BrokerListView.Items.Refresh();
            ToolListView.Items.Refresh();
        }

        void onToolModified()
        {
            BrokerListView.Items.Refresh();
            ToolListView.Items.Refresh();
        }
    }
}
