﻿using Microsoft.EntityFrameworkCore;
using PrimatLab_project.Commands;
using PrimatLab_project.Context;
using PrimatLab_project.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace PrimatLab_project.ViewModels
{
    class BrokerViewModel : ViewModelBase
    {
		public delegate void DialogClosed();
		public event DialogClosed Applied;

		private readonly LocalContext _context;

		private List<ToolModel> _tools;

		private readonly IEnumerable<string> _tickersList;

		public IEnumerable<string> TickersList => _tickersList;

		private BrokerModel _broker;

		public BrokerModel Broker
		{
			get
			{
				return _broker;
			}
			set
			{
				_broker = value;
				OnPropertyChanged(nameof(Broker));
			}
		}

		private string _name;
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
				_broker.Name = _name;
				OnPropertyChanged(nameof(Name));
			}
		}

		private string _country;
		public string Country
		{
			get
			{
				return _country;
			}
			set
			{
				_country = value;
				_broker.Country = _country;
				OnPropertyChanged(nameof(Country));
			}
		}

		private IList<string> _tool;
		public IList<string> Tool
		{
			get
			{
				return _tool;
			}
			set
			{
				_tool = value;
				_broker.Tools = _tools.FindAll(x => _tool.Contains(x.Ticker));
				_broker.Volume = _broker.Tools.Sum(x => x.Volume * x.LastPrice);
				OnPropertyChanged(nameof(Tool));
			}
		}

		private bool _access_flag;
		public bool AccessFlag
		{
			get
			{
				return _access_flag;
			}
			set
			{
				_access_flag = value;
				_broker.AccessFlag = _access_flag;
				OnPropertyChanged(nameof(AccessFlag));
			}
		}

		public ICommand ApplyCommand { get; }
		public ICommand CancelCommand { get; }



		public BrokerViewModel(BrokerModel subj, LocalContext context)
		{
			_context = context;
			_context.Tools.Load();
			_context.Brokers.Load();

			_tools = _context.Tools.Local.Where(x => x.BrokerModel == null).ToList();
			if(subj != null)
				_tools = _tools.Concat(subj.Tools).ToList();
			_tickersList = _tools.Select(tool => tool.Ticker);
			if ( subj ==null)
			{
				_broker = new BrokerModel();
				SubstituteNullBrokerModel();
				ApplyCommand = new RelayCommand(obj => { _context.Add(_broker); _context.SaveChanges(); }, canBeApplied);
			}
			else
			{
				_broker = _context.Brokers.FirstOrDefault(x => x == subj);
				_context.Update(_broker);
				SubstituteExistingBrokerModel(subj);
				ApplyCommand = new RelayCommand(obj => {_context.SaveChanges(); Applied?.Invoke(); }, canBeApplied);
			}
		}

		private void SubstituteExistingBrokerModel(BrokerModel broker)
		{
			_name = broker.Name;
			_country = broker.Country;
			_tool = broker.Tools.Select(x => x.Ticker).ToList();
			_access_flag = broker.AccessFlag;
		}

		private void SubstituteNullBrokerModel()
		{
			_name = "";
			_country = "";
			_access_flag = false;
		}

		private bool canBeApplied(object parameter)
		{
			BrokerModel broker = parameter is BrokerModel ? (BrokerModel)parameter : null;
			if (parameter == null)
				return false;
			return !(broker.Name == null || broker.Country == null || broker.Name == "" ||
						broker.Country == "" || broker.Tools == null || broker.Tools.Count == 0);
		}
	}
}
