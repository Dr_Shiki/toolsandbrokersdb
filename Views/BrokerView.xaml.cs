﻿using PrimatLab_project.Context;
using PrimatLab_project.Models;
using PrimatLab_project.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrimatLab_project.Views
{
    /// <summary>
    /// Interaction logic for BrokerView.xaml
    /// </summary>
    public partial class BrokerView : Window
    {
        public delegate void DialogClosed();
        public event DialogClosed Applied;

        public BrokerView(BrokerModel broker, LocalContext context)
        {
            InitializeComponent();

            TitleBorder.MouseLeftButtonDown += delegate { DragMove(); };

            DataContext = new BrokerViewModel(broker, context);


            (DataContext as BrokerViewModel).Applied += BrokerView_Applied;
        }


        private void BrokerView_Applied()
        {
            Applied?.Invoke();
        }
    }
}
